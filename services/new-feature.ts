export type NewStructure = {
  id: string
  someData: string
}

export function doSomethingRelatedToNewFeature(data: string) {
  const newStructure: NewStructure = {
    id: "123",
    someData: `__${data}`
  }
  return newStructure
}
