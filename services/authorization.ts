export type User = {
  role: "admin" | "guest"
}
export function isAuthorized(user: User): boolean {
  return user.role === "admin"
}
