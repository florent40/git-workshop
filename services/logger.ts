export function logMessage(message: string) {
  console.log(message)
}

export function logWarning(message: string) {
  console.log(`\x1b[33m⚠️  ${message}\x1b[0m`)
}



import { createInterface } from 'readline'
const readline = createInterface({
  input: process.stdin,
  output: process.stdout
});
export function input(message: string): Promise<string> {
  return new Promise<string>((resolve) => readline.question(`${message} `, resolve));
}
