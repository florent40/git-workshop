import { logMessage, logWarning, input } from './services/logger.js'
import { isAuthorized, User } from './services/authorization.js'
import { doSomethingRelatedToNewFeature } from './services/new-feature.js'

async function start() {
  logMessage("hello, world!")
  logWarning("this a demo, it could break at any time!")
  const username = await input("What's your name?")
  const role = await input("What's your role?")
  if (!isAuthorized({ role } as User)) {
    logMessage('You cannot continue this program. Sorry!')
    process.exit()
  }
  logMessage(`Welcome back, ${username}!`)
  const newFeatureData =  doSomethingRelatedToNewFeature("blabla")
  logMessage(newFeatureData.someData)
}

start();
